import mongoose, { Mongoose } from 'mongoose';

import { DbError } from '../shared/errors';

class dbConn {
    static conn: Mongoose;

    static async init() {
        if (this.conn === undefined) {
            try {
                this.conn = await mongoose.connect(
                    `mongodb://${process.env.DB_SERVERS}/${process.env.DB_NAME}`,
                );
            } catch (e) {
                throw new DbError(e);
            }
        }
        return this.conn;
    }
}

export { dbConn };
