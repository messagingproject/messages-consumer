import mongoose, { Schema } from 'mongoose';

import { DbError } from '../shared/errors';
import { dbConn } from './dbConn';

class MessagesDAL {
    static collection = 'messages';
    static MessageSchema = new Schema({
        text: String,
        createDate: Date,
    });
    static Message = mongoose.model(
        'Message',
        this.MessageSchema,
        this.collection,
    );

    static async insert(message: string) {
        await dbConn.init();
        const msg = new MessagesDAL.Message({
            text: message,
            createDate: new Date(),
        });
        try {
            await msg.save();
        } catch (e) {
            throw new DbError(e);
        }
    }
}

export { MessagesDAL };
