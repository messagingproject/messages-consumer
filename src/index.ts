import * as dotenv from 'dotenv';
import * as express from 'express';
import { Request, Response } from 'express';

import { KafkaService, MessagesService } from './services';

dotenv.config();

KafkaService.initKafka();
KafkaService.subscribeToMessages(MessagesService.handleNewMessage);

// app is listening for health check purposes only
const app = express();

app.get('/health', (req: Request, res: Response) =>
    res.send('Server is up and running'),
);

app.listen(process.env.PORT);
