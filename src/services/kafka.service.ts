import { Consumer, Kafka, Message, Producer } from 'kafkajs';

import { eMessageStatuses } from '../shared/enums';
import { KafkaError } from '../shared/errors';

class KafkaService {
    private static producer: Producer;
    private static consumer: Consumer;

    public static initKafka() {
        const kafka = new Kafka({
            clientId: process.env.KAFKA_CLIENT_ID,
            brokers: process.env.KAFKA_BROKERS.split(','),
        });

        this.initConsumer(kafka);
        this.initProducer(kafka);
    }

    private static async initConsumer(kafka: Kafka) {
        this.consumer = kafka.consumer({
            groupId: process.env.KAFKA_CONSUMER_GROUP_ID,
        });
        try {
            await this.consumer.connect();
        } catch (e) {
            throw new KafkaError(e);
        }
    }

    private static async initProducer(kafka: Kafka) {
        this.producer = kafka.producer();

        try {
            await this.producer.connect();
        } catch (e) {
            throw new KafkaError(e);
        }
    }

    private static async subscribeConsumer(
        topic: string,
        callbackFn: (msg: string, key?: string) => any,
    ) {
        try {
            await this.consumer.subscribe({ topic, fromBeginning: true });
            await this.consumer.run({
                eachMessage: async ({ message }) =>
                    callbackFn(
                        message.value.toString(),
                        message.key.toString(),
                    ),
            });
        } catch (e) {
            throw new KafkaError(e);
        }
    }

    private static async produceMany(
        messages: Partial<Message>[],
        topic: string,
    ) {
        try {
            this.producer.send({
                topic,
                messages: messages.map((msg) => ({
                    value: msg.value,
                    key: msg.key,
                })),
            });
        } catch (e) {
            throw new KafkaError(e);
        }
    }

    static subscribeToMessages(callbackFn: (msg: string, key?: string) => any) {
        return this.subscribeConsumer(process.env.KAFKA_MSG_TOPIC, callbackFn);
    }

    static produceStatus(status: eMessageStatuses, key: string) {
        return this.produceMany(
            [{ value: status.toString(), key }],
            process.env.KAFKA_MSG_STATUS_TOPIC,
        );
    }
}

export { KafkaService };
