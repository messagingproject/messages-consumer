import { MessagesDAL } from '../dal';
import { eMessageStatuses } from '../shared/enums';
import { KafkaService } from './kafka.service';

class MessagesService {
    static handleNewMessage = (msg: string, id: string) => {
        // Notice we can also have failures in inserting data to the failure queue -
        // This can be resolved using dead letter queue
        try {
            MessagesDAL.insert(msg);
            KafkaService.produceStatus(eMessageStatuses.Success, id);
        } catch (e) {
            KafkaService.produceStatus(eMessageStatuses.Failure, id);
        }
    };
}

export { MessagesService };
